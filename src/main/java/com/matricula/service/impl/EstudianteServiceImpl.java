package com.matricula.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.matricula.document.Estudiante;
import com.matricula.repo.IEstudianteRepo;
import com.matricula.repo.IGenericRepo;
import com.matricula.service.IEstudianteService;

@Service
public class EstudianteServiceImpl extends CRUDImpl<Estudiante, String> implements IEstudianteService {

  @Autowired
  private IEstudianteRepo repo;

  @Override
  protected IGenericRepo<Estudiante, String> getRepo() {
    return repo;
  }

}
