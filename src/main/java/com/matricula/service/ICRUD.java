package com.matricula.service;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.matricula.pagination.PageSupport;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ICRUD<T, ID> {

  Mono<T> registrar(T obj);

  Mono<T> modificar(T obj);

  Flux<T> listar();
  
  Flux<T> listar(Sort sort);
  
  Mono<PageSupport<T>> listarPage(Pageable page);
  
  Mono<PageSupport<T>> listarPage(Pageable page, Sort sort);

  Mono<T> listarPorId(ID id);

  Mono<Void> eliminar(ID id);
}
