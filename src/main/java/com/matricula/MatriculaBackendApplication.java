package com.matricula;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MatriculaBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(MatriculaBackendApplication.class, args);
	}

}
