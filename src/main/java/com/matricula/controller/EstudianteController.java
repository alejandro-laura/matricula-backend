package com.matricula.controller;

import static org.springframework.hateoas.server.reactive.WebFluxLinkBuilder.linkTo;
import static org.springframework.hateoas.server.reactive.WebFluxLinkBuilder.methodOn;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.matricula.document.Estudiante;
import com.matricula.pagination.PageSupport;
import com.matricula.service.IEstudianteService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/estudiantes")
public class EstudianteController {

  @Autowired
  private IEstudianteService service;

  @GetMapping("/listOrderDescEdad")
  public Mono<ResponseEntity<Flux<Estudiante>>> listarOrderDescEdad() {
    return Mono.just(ResponseEntity.ok()
        .contentType(MediaType.APPLICATION_JSON)
        .body(service.listar(Sort.by(Sort.Direction.DESC, "edad"))));
  }
  
  @GetMapping("/pageableOrderDescEdad")
  public Mono<ResponseEntity<PageSupport<Estudiante>>> listarPageableOrderDescEdad(
      @RequestParam(name = "page", defaultValue = "0") int page,
      @RequestParam(name = "size", defaultValue = "10") int size) {
    Pageable pageRequest = PageRequest.of(page, size);
    return service.listarPage(pageRequest, Sort.by(Sort.Direction.DESC, "edad")).map(p -> ResponseEntity.ok()
        .contentType(MediaType.APPLICATION_JSON)
        .body(p))
        .defaultIfEmpty(ResponseEntity.notFound().build());
  }
  
  @GetMapping
  public Mono<ResponseEntity<Flux<Estudiante>>> listar() {
    return Mono.just(ResponseEntity.ok()
        .contentType(MediaType.APPLICATION_JSON)
        .body(service.listar()));
  }

  @GetMapping("/{id}")
  public Mono<ResponseEntity<Estudiante>> listarPorId(@PathVariable("id") String id) {
    return service.listarPorId(id)
        .map(p -> ResponseEntity.ok()
            .contentType(MediaType.APPLICATION_JSON)
            .body(p))
        .defaultIfEmpty(ResponseEntity.notFound().build());
  }

  @PostMapping
  public Mono<ResponseEntity<Estudiante>> registrar(@Valid @RequestBody Estudiante estudiante, final ServerHttpRequest req) {
    return service.registrar(estudiante)
        .map(p -> ResponseEntity.created(URI.create(req.getURI().toString().concat("/").concat(p.getId())))
            .contentType(MediaType.APPLICATION_JSON)
            .body(p));
  }

  @PutMapping
  public Mono<ResponseEntity<Estudiante>> modificar(@Valid @RequestBody Estudiante estudiante) {
    return service.modificar(estudiante)
        .map(p -> ResponseEntity.ok()
            .contentType(MediaType.APPLICATION_JSON)
            .body(p));
  }

  @DeleteMapping("/{id}")
  public Mono<ResponseEntity<Void>> eliminar(@PathVariable("id") String id) {
    return service.listarPorId(id)
        .flatMap(p -> {return service.eliminar(p.getId())
            .then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT)));
    }).defaultIfEmpty(new ResponseEntity<Void>(HttpStatus.NOT_FOUND));
  }


  @GetMapping("/hateoas/{id}")
  public Mono<EntityModel<Estudiante>> listarHateoasPorId(@PathVariable("id") String id) {
    Mono<Link> link1 = linkTo(methodOn(EstudianteController.class).listarHateoasPorId(id)).withSelfRel().toMono();
    return service.listarPorId(id)
        .zipWith(link1, (p, links) -> EntityModel.of(p, links));
  }

  @GetMapping("/pageable")
  public Mono<ResponseEntity<PageSupport<Estudiante>>> listarPageable(
      @RequestParam(name = "page", defaultValue = "0") int page,
      @RequestParam(name = "size", defaultValue = "10") int size) {
    Pageable pageRequest = PageRequest.of(page, size);
    return service.listarPage(pageRequest).map(p -> ResponseEntity.ok()
        .contentType(MediaType.APPLICATION_JSON)
        .body(p))
        .defaultIfEmpty(ResponseEntity.notFound().build());
  }
  
}
