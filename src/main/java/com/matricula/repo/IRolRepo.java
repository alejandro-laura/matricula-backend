package com.matricula.repo;

import com.matricula.document.Rol;

public interface IRolRepo extends IGenericRepo<Rol, String> {

}
