package com.matricula.repo;

import com.matricula.document.Estudiante;

public interface IEstudianteRepo extends IGenericRepo<Estudiante, String> {

}
