package com.matricula.repo;

import com.matricula.document.Curso;

public interface ICursoRepo extends IGenericRepo<Curso, String> {

}
