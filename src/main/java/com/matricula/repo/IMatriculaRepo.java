package com.matricula.repo;

import com.matricula.document.Matricula;

public interface IMatriculaRepo extends IGenericRepo<Matricula, String> {

}
